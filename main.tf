resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = var.vpc_tag
  }
}



resource "aws_subnet" "pub_sub" {
  #count = length(var.pub_sub_cidr)
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.pub_sub_cidr

  tags = {
    Name = var.pub_sub_tag
  }
}

resource "aws_subnet" "priv_sub" {
  #count = length(var.priv_sub_cidr)
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.priv_sub_cidr

  tags = {
    name = var.priv_sub_tag
  }
}

resource "aws_internet_gateway" "igw1" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = var.internet_gateway
  }
}

resource "aws_eip" "elasticip" {
  vpc = true
  tags = {
    Name = var.elastic
  }
}

resource "aws_nat_gateway" "nat1" {
  allocation_id = aws_eip.elasticip.id
  subnet_id     = aws_subnet.pub_sub.id
  tags = {
    Name = var.nat_gateway
  }
}


resource "aws_route_table" "igw_route" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw1.id
  }
  tags = {
    Name = var.igw_rt
  }
}

resource "aws_route_table_association" "pub_route" {
  # count = length(var.pub_sub_cidr)
  subnet_id      = aws_subnet.pub_sub.id
  route_table_id = aws_route_table.igw_route.id
}

resource "aws_route_table" "nat_route" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat1.id
  }
  tags = {
    Name = var.nat_rt
  }
}

resource "aws_route_table_association" "priv_route" {
  # count = length(var.priv_sub_cidr)
  subnet_id      = aws_subnet.priv_sub.id
  route_table_id = aws_route_table.nat_route.id
}


#bastion host
resource "tls_private_key" "new_key2" {
  algorithm = "RSA"
  rsa_bits  = "2048"
}

resource "aws_key_pair" "mykey2" {
  key_name   = "mykey2"
  public_key = tls_private_key.new_key2.public_key_openssh
}
resource "local_file" "private_key" {
  depends_on = [
    tls_private_key.new_key2,
  ]
  content  = tls_private_key.new_key2.private_key_pem
  filename = "key.pem"
}

# bastion host
resource "aws_security_group" "bastion_default_rules" {
  name   = var.bastion_sg
  vpc_id = aws_vpc.vpc.id
  dynamic "ingress" {
    for_each = var.bastion_ports
    content {
      from_port   = ingress.key
      to_port     = ingress.key
      cidr_blocks = [ingress.value]
      protocol    = "tcp"
    }
  }
  tags = {
    Name = var.bastion_sg
  }
}

resource "aws_instance" "bastion" {
  ami                         = var.ubuntu_ami
  instance_type               = "t3.micro"
  subnet_id                   = aws_subnet.pub_sub.id
  associate_public_ip_address = true
  key_name                    = aws_key_pair.mykey2.key_name
  vpc_security_group_ids      = [aws_security_group.bastion_default_rules.id]
  tags = {
    Name = var.bastion_name
  }
}

#resource "aws_security_group" "node_default_rules" {
#  name   = var.node_sg
#  vpc_id = aws_vpc.vpc.id
#  dynamic "ingress" {
#    for_each = var.node_ports
#    content {
#      from_port   = ingress.key
#      to_port     = ingress.key
#      cidr_blocks = [ingress.value]
#      protocol    = "tcp"
#    }
#  }
#  tags = {
#    Name = var.node_sg
#  }
#}

resource "aws_security_group" "node_sg" {
  name        = var.node_sg
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.vpc.id
  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.247/32"] #allow vpc network
  }


  tags = {
    Name = var.node_sg
  }
}





resource "aws_instance" "node" {
  ami                         = var.ubuntu_ami
  instance_type               = "t3.micro"
  subnet_id                   = aws_subnet.priv_sub.id
  associate_public_ip_address = false
  key_name                    = aws_key_pair.mykey2.key_name
  vpc_security_group_ids      = [aws_security_group.node_sg.id]
  tags = {
    Name = var.node_name
  }
}

resource "aws_security_group" "node_default_rules" {
  name        = var.client_sg
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.vpc.id
  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["103.92.43.202/32"] #allow vpc network
  }


  tags = {
    Name = var.client_sg
  }
}

resource "aws_instance" "client" {
  ami                         = var.ubuntu_ami
  instance_type               = "t3.micro"
  subnet_id                   = aws_subnet.priv_sub.id
  associate_public_ip_address = false
  key_name                    = aws_key_pair.mykey2.key_name
  vpc_security_group_ids      = [aws_security_group.node_default_rules.id]
  tags = {
    Name = var.client_name
  }
}
