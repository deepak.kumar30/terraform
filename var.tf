variable "vpc_cidr" {
  description = "VPC CIDR of Ninja-vpc-01"
  type        = string
  default     = "10.0.0.0/16"
}

variable "vpc_tag" {
  description = "Tags of VPC"
  type        = string
  default     = "ninja-vpc-01"
}

variable "pub_sub_cidr" {
  description = "Public Subnet CIDR"
  type        = string
  default     = "10.0.0.0/24"
}

variable "pub_sub_tag" {
  description = "Tags of Public Subnet"
  type        = string
  default     = "bastion-host"
}

variable "priv_sub_cidr" {
  description = "Private Subnet CIDR"
  type        = string
  default     = "10.0.1.0/24"
}

variable "priv_sub_tag" {
  description = "Tags of Private Subnet"
  type        = string
  default     = "Vault-server"
}

variable "internet_gateway" {
  description = "Internet gateway Tag"
  type        = string
  default     = "ninja-igw-01"
}

variable "nat_gateway" {
  description = "NAT Gateway"
  type        = string
  default     = "ninja-nat-01"
}

variable "igw_rt" {
  default = "ninja-route-pub-01"
}

variable "nat_rt" {
  default = "ninja-route-priv-01"
}

variable "elastic" {
  default = "new-elastic-ip"
}

variable "bastion_name" {
  type    = string
  default = "bastion-host"
}

variable "ubuntu_ami" {
  default = "ami-096c4b6e0792d8c16"
}

variable "bastion_ports" {
  description = "Allowed bastion ports"
  type        = map(any)
  default = {
    "22" = "103.92.43.202/32"
  }
}

variable "bastion_sg" {
  default = "bastion_sg_01"
}

#variable "node_ports" {
#  description = "Allowed bastion ports"
#  type        = map(any)
#  default = {
#    #"22"  = aws_instance.bastion.instances.private_ip"/32" 
#  }
#}

variable "node_sg" {
  default = "node_sg_01"
}

variable "node_name" {
  default = "vaultserver-instance"
}

variable "key_n" {
  default = "key.pem"
}

variable "client_sg" {
  default = "client_sg_01"
}

variable "client_name" {
  type    = string
  default = "Vault-Client"
}
